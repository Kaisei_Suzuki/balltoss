﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallGenerator : MonoBehaviour
{
    public GameObject WallPrefab;
    float span = 0.2f;
    float delta = 0;
    

    void Update()
    {
        float px = Random.Range(-7, 7);
        float py = Random.Range(3.0f, -2.0f);
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(WallPrefab) as GameObject;
            go.transform.position = new Vector3(px, py, 0);
        }
        
    }
    
}
