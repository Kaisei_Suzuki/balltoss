﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallGenerator : MonoBehaviour
{

    public GameObject BallPrefab;

    public void Shoot(Vector3 dir)
    {
        GetComponent<Rigidbody>().AddForce(dir);
    }
    void Start()
    {
        Shoot(new Vector3(0, 200, 2000));
    }

    void Update()
    {

        if (Input.GetKey(KeyCode.Space))
        {
            GameObject go = Instantiate(BallPrefab) as GameObject;
            Shoot(new Vector3(0, 200, 2000));

        }
    }
}
