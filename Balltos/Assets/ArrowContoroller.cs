﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowContoroller : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
            this.transform.Rotate(0.0f, 0.0f, 2.0f);    //今の状態からＺ軸で2°回転
        if (Input.GetKey(KeyCode.LeftArrow))
            this.transform.Rotate(0.0f, 0.0f, -2.0f);    //今の状態からＺ軸で-2°回転
    }
}
