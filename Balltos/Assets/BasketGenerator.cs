﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasketGenerator : MonoBehaviour
{
    public GameObject BasketPrefab;
    float span = 0.8f;
    float delta = 0;

 void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)
        {
            this.delta = 0;
            GameObject go = Instantiate(BasketPrefab) as GameObject;
            go.transform.position = new Vector3(-10, -3.5f, 0);
        }
    }
}
