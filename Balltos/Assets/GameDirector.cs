﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameDirector : MonoBehaviour
{
    GameObject timer;

    // Start is called before the first frame update
    void Start()
    {
        this.timer = GameObject.Find("timer");
    }



    public void DecreaseHp()
    {
        timer.GetComponent<Image>().fillAmount -= 0.1f;
    }
}